import java.io.*;

public class ByteArraySerializer<T extends Serializable> {
    public byte[] serialize(final T obj) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);

        oos.writeObject(obj);
        oos.flush();

        return baos.toByteArray();
    }

    public T deserialize(final byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        ObjectInputStream ois = new ObjectInputStream(bais);

        return (T)ois.readObject();
    }
}
