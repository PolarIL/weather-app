import java.io.InputStream;
import java.util.Scanner;

public abstract class InputOperation<T> {

    protected final InputStream stream;
    protected final Scanner scanner;

    InputOperation(InputStream stream) {
        this.stream = stream;
        this.scanner = new Scanner(stream);
    }

    abstract T op() throws Exception;

    T tryTimes(int tries) throws Exception {
        for (int i = 0; i < tries; ++i) {
            try {
                return op();
            }
            catch (Exception e) {
                if (i == tries - 1) {
                    throw e;
                }
            }
        }

        // Make compiler shut up about missing return statement,
        // code will never be reached.
        return null;
    }

    T tryIndefinitely() {
        while (true) {
            try {
                return op();
            }
            catch (Exception e) {}
        }
    }
}
