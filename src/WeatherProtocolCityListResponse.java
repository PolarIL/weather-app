public class WeatherProtocolCityListResponse implements WeatherProtocolResponse {
    public final WeatherCityList cityList;

    public WeatherProtocolCityListResponse(final WeatherCityList cityList) {
        this.cityList = cityList;
    }

    public WeatherCityList getCityList() { return new WeatherCityList(cityList); }
}
