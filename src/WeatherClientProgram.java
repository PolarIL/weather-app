import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.*;
import java.util.ListIterator;

public class WeatherClientProgram {
    private static int INPUT_TRIES = 10;

    public static void main(String[] args) {
        final WeatherServer server = getServerInputs(System.in, System.out);
        runClient(server, System.in, System.out);
    }

    private static void runClient(final WeatherServer server, final InputStream in, final PrintStream out) {
        out.println("Attempting to fetch city data from " + server + "...");

        try {
            WeatherCityList cityList = WeatherCityList.fetchCityListFrom(server);
            String cityName = cityList.getCity(presentCityChoice(cityList, in, out));
            WeatherCityForecast cityForecast = WeatherProtocol.fetchCityForecastFrom(server, cityName);
            presentCityForecast(cityForecast, out);
        }
        catch (IOException e) {
            out.println("Unable to communicate with server at " + server + ", exiting...");
            System.exit(2);
        }
        catch (WeatherProtocol.WeatherProtocolException e) {
            out.println(e.getMessage());
        }

        out.println("Have a nice day!");
    }

    private static int presentCityChoice(final WeatherCityList cityList, final InputStream in, final PrintStream out) {
        out.println("Please select a city from the available city list by typing the corresponding number:");

        ListIterator it = cityList.getCities().listIterator();

        while (it.hasNext())
            out.println("\t(" + (it.nextIndex() + 1)+ "): " + it.next());

        int cityIndex;
        do {
            cityIndex = getCityInput(in, out);
        } while (cityIndex > cityList.getLength());

        return cityIndex - 1;
    }

    private static void presentCityForecast(final WeatherCityForecast cityForecast, final PrintStream out) {
        out.println("The forecast for " + cityForecast.getName() + " is:");
        out.println("\tToday: " + cityForecast.getToday());
        out.println("\tTomorrow: " + cityForecast.getTomorrow());
        out.println("\tTwo days from now: " + cityForecast.getTwoDays());
    }

    private static int getCityInput(final InputStream in, final PrintStream out) {
        InputOperation<Integer> inputCityChoice = new InputOperation<Integer>(in) {
            @Override
            Integer op() throws Exception {
                out.print("> ");
                return Integer.parseUnsignedInt(scanner.next());
            }
        };

        return inputCityChoice.tryIndefinitely();
    }

    private static WeatherServer getServerInputs(final InputStream in, final PrintStream out) {
        InputOperation<InetAddress> inputHostname = new InputOperation<InetAddress>(in) {
            @Override
            InetAddress op() throws Exception {
                out.print("Server host name/IP: ");
                return InetAddress.getByName(scanner.nextLine());
            }
        };

        InputOperation<Integer> inputPort = new InputOperation<Integer>(in) {
            @Override
            Integer op() throws Exception {
                out.print("Server host port: ");
                return Integer.parseUnsignedInt(scanner.next());
            }
        };

        try {
            return new WeatherServer(inputHostname.tryTimes(INPUT_TRIES), inputPort.tryTimes(INPUT_TRIES));
        }
        catch (Exception e) {
            out.println("Exiting...");
            System.exit(1);
        }

        // Make compiler shut up.
        return null;
    }
}