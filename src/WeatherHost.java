import java.net.InetAddress;

public class WeatherHost {
    private InetAddress hostname;
    private int port;

    public WeatherHost(final InetAddress hostname, int port) {
        this.hostname = hostname;
        this.port = port;
    }

    public InetAddress getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    public String toString() {
        return hostname + ":" + port;
    }
}