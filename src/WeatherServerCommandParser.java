public class WeatherServerCommandParser {
    private static Class[] commandList = {WeatherServerCommandExit.class, WeatherServerCommandReload.class};

    public static WeatherServerCommand parseCommand(final String input) throws Exception {
        for (Class c : commandList) {
            try {
                return (WeatherServerCommand)c.getConstructors()[0].newInstance(input);
            }
            catch (Exception e) {}
        }

        throw new Exception("Invalid command " + input.split(" ")[0] + "!");
    }
}