import java.io.File;

public class WeatherServerCommandReload extends WeatherServerCommand {
    private String fileName;

    public WeatherServerCommandReload(final String input) throws Exception {
        String[] s = input.split(" ");

        if (!s[0].toLowerCase().equals("reload") || s.length != 2)
            throw new Exception("Invalid reload command!");

        fileName = s[1];
    }

    public void runCommand() {
        WeatherServerProgram.loadWeatherDataFromXML(new File(fileName));
    }
}
