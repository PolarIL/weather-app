public interface WeatherProtocolRequest extends WeatherProtocolPacket {
    public void visitProcess(final WeatherClient client);
}
