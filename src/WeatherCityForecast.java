import java.io.Serializable;

public class WeatherCityForecast implements Serializable {
    private String name;
    private String today;
    private String tomorrow;
    private String twoDays;

    public WeatherCityForecast(final String name, final String today, final String tomorrow, final String twoDays) {
        this.name = name;
        this.today = today;
        this.tomorrow = tomorrow;
        this.twoDays = twoDays;
    }

    public WeatherCityForecast(final WeatherCityForecast other) {
        this(other.getName(), other.getToday(), other.getTomorrow(), other.getTwoDays());
    }

    public String getName() {
        return name;
    }

    public String getToday() {
        return today;
    }

    public String getTomorrow() {
        return tomorrow;
    }

    public String getTwoDays() {
        return twoDays;
    }

    public String toString() {
        return getName() + ":" + getToday() + "/" + getTomorrow() + "/" + getTwoDays();
    }
}
