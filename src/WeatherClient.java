import java.net.InetAddress;

public class WeatherClient extends WeatherHost {
    public WeatherClient(final InetAddress hostname, int port) {
        super(hostname, port);
    }
}
