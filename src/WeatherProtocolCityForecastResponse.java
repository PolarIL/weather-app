public class WeatherProtocolCityForecastResponse implements WeatherProtocolResponse {
    private WeatherCityForecast cityForecast;

    public WeatherProtocolCityForecastResponse(final WeatherCityForecast cityForecast) {
        this.cityForecast = new WeatherCityForecast(cityForecast);
    }

    public WeatherCityForecast getCityForecast() {
        return cityForecast;
    }
}
