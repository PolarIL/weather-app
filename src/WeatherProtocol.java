import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class WeatherProtocol {
    public static int MAX_RECV_BYTES = 8 * 1024;
    public static int MAX_RECV_TIMEOUT = 10 * 1000;

    public static class WeatherProtocolException extends Exception {
        public WeatherProtocolException(String message) {
            super(message);
        }
    }

    private static DatagramSocket newSocket() throws IOException {
        DatagramSocket socket = new DatagramSocket();
        socket.setSoTimeout(MAX_RECV_TIMEOUT);

        return socket;
    }

    private static void sendRequest(final WeatherServer server, final DatagramSocket socket, final WeatherProtocolRequest request) throws IOException {
        byte[] sendData = new ByteArraySerializer<WeatherProtocolRequest>().serialize(request);
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, server.getHostname(), server.getPort());
        socket.send(sendPacket);
    }

    private static void sendResponse(final WeatherClient client, final DatagramSocket socket, final WeatherProtocolResponse response) throws IOException {
        byte[] sendData = new ByteArraySerializer<WeatherProtocolResponse>().serialize(response);
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, client.getHostname(), client.getPort());
        socket.send(sendPacket);
    }

    public static void sendResponseCityList(final WeatherClient client, final WeatherCityList cityList) throws IOException {
        sendResponse(client, newSocket(), new WeatherProtocolCityListResponse(cityList));
    }

    public static void sendResponseCityForecast(final WeatherClient client, final WeatherCityForecast cityForecast) throws IOException {
        sendResponse(client, newSocket(), new WeatherProtocolCityForecastResponse(cityForecast));
    }

    public static void sendResponseInvalidRequest(final WeatherClient client) throws IOException {
        sendResponse(client, newSocket(), new WeatherProtocolInvalidRequestResponse());
    }

    public static WeatherCityList fetchCityListFrom(final WeatherServer server) throws IOException, WeatherProtocolException {
        DatagramSocket socket = newSocket();

        sendRequest(server, socket, new WeatherProtocolCityListRequest());

        // Receive response
        try {
            WeatherProtocolResponse response = receiveResponse(server, socket);
            return ((WeatherProtocolCityListResponse) response).getCityList();
        }
        catch (ClassNotFoundException e) {
            throw new WeatherProtocolException("Server " + server + " sent an invalid response!");
        }
    }

    public static WeatherCityForecast fetchCityForecastFrom(final WeatherServer server, final String cityName) throws IOException, WeatherProtocolException {
        DatagramSocket socket = newSocket();

        sendRequest(server, socket, new WeatherProtocolCityForecastRequest(cityName));

        try {
            WeatherProtocolResponse response = receiveResponse(server, socket);
            return ((WeatherProtocolCityForecastResponse) response).getCityForecast();
        }
        catch (ClassNotFoundException e) {
            throw new WeatherProtocolException("Server " + server + " sent an invalid response!");
        }
    }

    private static WeatherProtocolResponse receiveResponse(final WeatherServer server, final DatagramSocket socket) throws IOException, ClassNotFoundException {
        byte[] recvData = new byte[MAX_RECV_BYTES];
        DatagramPacket recvPacket = new DatagramPacket(recvData, recvData.length, server.getHostname(), server.getPort());
        socket.receive(recvPacket);
        return new ByteArraySerializer<WeatherProtocolResponse>().deserialize(recvData);
    }
}