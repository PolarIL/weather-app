import java.net.InetAddress;

public class WeatherServer extends WeatherHost {
    public WeatherServer(final InetAddress hostname, int port) {
        super(hostname, port);
    }
}
