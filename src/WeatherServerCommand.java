public abstract class WeatherServerCommand {
    public abstract void runCommand();
}
