import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Scanner;

public class WeatherServerProgram {
    public static final int LOCAL_PORT = 8000;

    public static HashMap<String, WeatherCityForecast> cityForecastMap;

    private static DatagramSocket openSocket() {
        try {
            return new DatagramSocket(LOCAL_PORT, InetAddress.getLoopbackAddress());
        }
        catch (IOException e) {
            unableToCommunicate();
        }

        // Shut up compiler
        return null;
    }

    private static void listenForRequests() {
        final DatagramSocket socket = openSocket();

        WeatherHost server = new WeatherHost(socket.getLocalAddress(), socket.getLocalPort());
        System.out.println("Listening for clients on " + server + "...");

        Thread listenThread = new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] data = new byte[WeatherProtocol.MAX_RECV_BYTES];
                DatagramPacket packet = new DatagramPacket(data, data.length);

                while (true) {
                    waitForRequest(socket, packet);
                }
            }
        });

        listenThread.start();
    }

    private static void waitForRequest(final DatagramSocket socket, final DatagramPacket recvPacket) {
        try {
            socket.receive(recvPacket);
            processRequestPacket(recvPacket);
        }
        catch (IOException e) {
            unableToCommunicate();
        }
    }

    private static void processRequestPacket(final DatagramPacket recvPacket) {
        WeatherClient client = new WeatherClient(recvPacket.getAddress(), recvPacket.getPort());

        try {
            WeatherProtocolRequest request = new ByteArraySerializer<WeatherProtocolRequest>().deserialize(recvPacket.getData());
            WeatherServerRequestProcessor.process(client, request);
        }
        catch (IOException e) {
            logWarning("Unable to deserialize request from " + client + "!");
        }
        catch (ClassNotFoundException e) {
            logWarning("Client " + client + " sent an unknown request!");

            try {
                WeatherProtocol.sendResponseInvalidRequest(client);
            }
            catch (IOException ioe) {
                unableToCommunicate();
            }
        }
    }

    private static void listenForCommands() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("serverCommand> ");

            processCommand(scanner.nextLine());
        }
    }

    public static void processCommand(final String input) {
        try {
            WeatherServerCommandParser.parseCommand(input).runCommand();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void loadWeatherDataFromXML(final File inputFile) {
        HashMap<String, WeatherCityForecast> map = new HashMap<String, WeatherCityForecast>();

        try {
            cityForecastMap = XMLCityForecastParser.parseXMLFile(inputFile);
        }
        catch (Exception e) {
            System.out.println("Exception thrown while loading XML data: " + e);
            System.exit(3);
        }
    }

    public static void main(String[] args) {
        System.out.println("Weather server loading...");

        loadWeatherDataFromXML(new File("cityData.xml"));
        listenForRequests();
        listenForCommands();
    }

    private static void unableToCommunicate() {
        System.out.println("Unable to listen for clients, exiting...");
        System.exit(1);
    }

    public static void logWarning(final String warning) {
        System.out.println("Warning: " + warning);
    }

}