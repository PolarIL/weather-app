import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

class XMLCityForecastParser {
    private final String cityName;
    private final WeatherCityForecast cityForecast;

    public XMLCityForecastParser(final Element cityElement) {
        cityName = cityElement.getAttribute("name");
        String cityToday = cityElement.getElementsByTagName("today").item(0).getTextContent();
        String cityTomorrow = cityElement.getElementsByTagName("tomorrow").item(0).getTextContent();
        String cityTwoDays = cityElement.getElementsByTagName("twodays").item(0).getTextContent();

        cityForecast = new WeatherCityForecast(cityName, cityToday, cityTomorrow, cityTwoDays);
    }

    public static HashMap<String, WeatherCityForecast> parseXMLFile(final File inputFile) throws SAXException, IOException, ParserConfigurationException {
        HashMap<String, WeatherCityForecast> map = new HashMap<String, WeatherCityForecast>();

        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputFile);
        doc.getDocumentElement().normalize();

        NodeList nodeList = doc.getElementsByTagName("city");

        for (int i = 0; i < nodeList.getLength(); ++i) {
            Element cityElement = (Element)nodeList.item(i);
            XMLCityForecastParser XMLCityForecastExtractor = new XMLCityForecastParser(cityElement);
            String cityName = XMLCityForecastExtractor.getCityName();
            WeatherCityForecast cityForecast = XMLCityForecastExtractor.getCityForecast();

            System.out.println(cityForecast);

            map.put(cityName, cityForecast);
        }

        return map;
    }

    public String getCityName() {
        return cityName;
    }

    public WeatherCityForecast getCityForecast() {
        return cityForecast;
    }
}