import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WeatherCityList implements Serializable {
    private ArrayList<String> cities;

    public static WeatherCityList fetchCityListFrom(final WeatherServer server) throws IOException, WeatherProtocol.WeatherProtocolException {
        return WeatherProtocol.fetchCityListFrom(server);
    }

    WeatherCityList(final List<String> cities) {
        this.cities = new ArrayList<String>(cities);
    }

    WeatherCityList(final WeatherCityList other) {
        this(other.cities);
    }

    public List<String> getCities() {
        return new ArrayList<String>(cities);
    }

    public int getLength() {
        return cities.size();
    }

    public String getCity(int index) {
        return cities.get(index);
    }
}
