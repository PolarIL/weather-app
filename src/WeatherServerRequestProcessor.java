import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class WeatherServerRequestProcessor {
    public static void process(final WeatherClient client, final WeatherProtocolRequest request) {
        request.visitProcess(client);
    }

    // Actual processing functions
    public static void processConcrete(final WeatherClient client, final WeatherProtocolCityListRequest request) {
        WeatherCityList cityList = transformCityMapToList(WeatherServerProgram.cityForecastMap);

        try {
            WeatherProtocol.sendResponseCityList(client, cityList);
        }
        catch (IOException e) {
            WeatherServerProgram.logWarning("Unable to send response to client " + client + "!");
        }
    }

    public static void processConcrete(final WeatherClient client, final WeatherProtocolCityForecastRequest request) {
        WeatherCityForecast cityForecast = WeatherServerProgram.cityForecastMap.get(request.getCityName());

        try {
            if (cityForecast == null)
                WeatherProtocol.sendResponseInvalidRequest(client);
            else
                WeatherProtocol.sendResponseCityForecast(client, cityForecast);
        }
        catch (IOException e) {
            WeatherServerProgram.logWarning("Unable to send response to client " + client + "!");
        }
    }

    // Helper methods
    private static WeatherCityList transformCityMapToList(final HashMap<String, WeatherCityForecast> map) {
        ArrayList<String> cityList = new ArrayList<String>(map.size());
        cityList.addAll(map.keySet());

        return new WeatherCityList(cityList);
    }
}
