public class WeatherProtocolCityListRequest implements WeatherProtocolRequest {
    // Visitor pattern boilerplate...
    public void visitProcess(final WeatherClient client) {
        WeatherServerRequestProcessor.processConcrete(client, this);
    }
}
