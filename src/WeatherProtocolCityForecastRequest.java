public class WeatherProtocolCityForecastRequest implements WeatherProtocolRequest {
    private String cityName;

    public WeatherProtocolCityForecastRequest(final String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    // Visitor pattern boilerplate...
    public void visitProcess(final WeatherClient client) {
        WeatherServerRequestProcessor.processConcrete(client, this);
    }
}