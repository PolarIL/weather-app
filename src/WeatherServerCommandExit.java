public class WeatherServerCommandExit extends WeatherServerCommand {
    public WeatherServerCommandExit(final String input) throws Exception {
        if (!input.toLowerCase().startsWith("exit"))
            throw new Exception();
    }

    public void runCommand() {
        System.out.println("Weather server exiting on user request...");
        System.exit(0);
    }
}